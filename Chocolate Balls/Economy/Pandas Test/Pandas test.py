import pandas as pd
import numpy as np

economy=pd.read_csv("Economy_raw data.csv")

GDP_countries=["Country Name", "Trade (%of GDP)", "Resources (% of GDP)"]
columns=list(economy.columns)

GDP_dependant=economy[GDP_countries] #indicators with GDP as component


def col_average (datafrm, col_name): #creates average from a column
    col_value=datafrm[[col_name]]
    nums=col_value.loc[1:len(col_value)]
    y=np.mean(nums)
    x=float(y)
    return x

#average values
average_trade=col_average(economy, "Trade (%of GDP)")
average_res=col_average(economy, "Resources (% of GDP)")

resource_based=pd.DataFrame
trade_based=[]
        
#x=economy.loc[0:10][]
economy.set_index("Country Name", inplace = True)

print (economy.loc["Poland"])