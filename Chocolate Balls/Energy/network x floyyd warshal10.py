import matplotlib.pyplot as plt
import networkx as nx
import math as ma

G=nx.path_graph([5])
H=nx.path_graph([1])
P=nx.path_graph(12)

#P=nx.shortest_path

#land
weighta=1
#mountain
weightb=2
#sea
weightc=3

G.add_edges_from([(0,1),(1,2),(2,3),(3,4),(4,5),(5,0)])
G.add_edges_from([(1,4),(2,5),(3,0)])

Q=nx.floyd_warshall_predecessor_and_distance
#P=nx.floyd_warshall(G, weight=1>3)

I=G.add_edge(0,1,weight=6)
J=G.add_edge(1,2,weight=3)
K=G.add_edge(2,3,weight=2)
L=G.add_edge(3,4,weight=2)
M=G.add_edge(4,5,weight=1)
N=G.add_edge(5,0,weight=3)

G.add_edge(1,4,weight=3)
G.add_edge(2,5,weight=1)
G.add_edge(3,0,weight=2)



#nx.draw_networkx(G)
#nx.draw_networkx(H)

#nx.draw_spectral(G)
#nx.draw_spectral(H)

#nx.draw_spring(G)
#nx.draw_spring(H)

#nx.draw(G)
nx.draw(H)
nx.draw(P)  
#nx.draw(P)  
      
nx.draw_circular(G)
#nx.draw_circular(H)

print (Q) 
